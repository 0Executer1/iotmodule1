# INDUSTRY 

![industrial revolution](https://www.netobjex.com/wp-content/uploads/2018/12/1-1.png)

Industries play an important role in the economic development of their countries. Due to this industrial revolution is going on continuously and the image describes the 4 main stages of that revolution.

## Industry 1.0 :
  This is the starting stage of industrial revolution where mechanisation and usage of steam power mainly developed in this stage.

## Industry 2.0 :
  Here we get into mass production and efficient utilisation of electrical energy.

## Industry 3.0 :
  This is the stage where the game changer enters to industries and those are computers. And automation of machines had been developed by electronics  and communication became more efficient with the machines used in industries.

### Industry 3.0 architecture:
 ![i3_arch](pics/I3_arch.png)

 Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data. Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts.

### Industry 3.0 Communication protocols:
 ![i3_protocol](pics/i3_protocols.png) 
 
 All these protocols are optimized for sending data to a central server inside the factory.<br/>
These protocols are used by sensors to Send data to PLC's. These protocols are called as fieldbus.


## Industry 4.0 :
 Yes!! now what we are seeing the industries are in this stage, where we store the data of all the divisions of industry 3.0 on internet and access it efficiently to maximise the productivity, efficiency and profit for the industries, this process is called as Industrial Internet of Things(IIoT). And also cyber physical systems(CPS) and cloud computing also implemented on manufacturing processes.
This technology's impact include:
* Interoperability
* Decentralization of information
* Real-time data collection
* Heightened flexibility <br/>
As such, the difference between Industry 3.0 and Industry 4.0 is the presence of new interconnected technologies in plant operations.

 ![3.o to 4.0](https://qph.fs.quoracdn.net/main-qimg-25782847f73e3000e5dd12704168ad18.webp)

 ### Industry 4.0 architecture:
 ![i4_arch](pics/I4_arch.png)

 Data data goes from Controller to Cloud via the Industry 4.0 protocols.

 ### Industry 4.0 Communication protocols:
 ![i4_protocols](pics/I4_protocol.png)

 All these protocols are optimized for sending data to cloud for data analysis.

 ## How to convert Industry 3.0 to Industry 4.0?
 ![i3_to_i4](pics/I3_to_i4.png)

 ## Now after sending the data to the Internet, What next?
   
   Analysis that data using various tools available online.

 ### TSDB tools:
 ![TSDB tools](pics/TSDB_tools.png)
<br/>

  These above tools stores the data in Time Series Databases.

 ### IOT Dashboard:
 ![Dashboard](https://miro.medium.com/max/3694/1*KimwgjULRZzONpjGFH1sTA.png)
 <br/>

 This above image explains, how the IOT dashboard's look.
   
 ### IOT platforms:
 ![platforms](pics/IOT_platforms.png)
<br/>

 The data gets analysised on the above respective platforms.

 ### Get Alerts:
 ![Alerts](pics/Alerts.png)
<br/>

 We will get alerts based on our data from the above respective platforms.  

## What is the difference between Industry 3.0 and Industry 4.0?

In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor.

For an example of the old way (Industry 3.0), take a CNC machine: while largely automated, it still needs input from a human controller. The process is automated based on human input, not by data. Under Industry 4.0, that same CNC machine would not only be able to follow set programming parameters, but also use data to streamline production processes.

![CNC machine](https://miro.medium.com/max/1600/1*X-Uw98xq0VlRpgKlQxkjow.gif)  

CNC machines are electromechanical devices that can manipulate tools on a varying number of axis with high precision to create a part as per instruction put in via a computer program. They run faster than any other manually-operated machine and can hence produce more objects with high precision from any design.

### How will CNC and IIoT Perform and Behave Collectively?

While at a lower level the implementation of IoT enables the use of CNC machine monitoring solutions, on a larger scale IIoT creates machine-to-machine interaction that results in automated operations and less manual intervention.

Here are some benefits that a company can enjoy from this promising conglomeration of CNC and IIoT.

1. Scheduling Maintenance:<br/>
  IIoT allows operators and machine handlers to conveniently interact with their CNC machines in many ways through smartphones or tablets. From IoT based CNC machine monitoring, they can, therefore, track the condition of machines at all times from a distance.

2.  Machine Uptime Monitoring:<br/> 
  Capabilities of IoT based CNC solutions are not only limited to remote monitoring and scheduled maintenance for CNC machines. By using IoT systems and leveraging its real-time alert feature, factories can now reduce their machine downtime and increase overall equipment effectiveness. 

3.  Improved Worker Safety:<br/>
  For a larger implementation, the technology of IIoT can also be used to reduce manual efforts, or in other words, remove the potential of workers’ injury from the equation of a factory’s operation.

Even though the CNC machines are themselves capable of uplifting a machine to new heights, by using the technology of the Industrial Internet of Things, the capabilities of a company can be increased even further. CNC machine monitoring solution is a perfect example that shows the capability of IoT based CNC solutions.

## What will Industry 4.0 look like?

In terms of maintenance, Industry 4.0 means lots of data can be collected through sensors and used to make decisions about repairs and upkeep. Predictive maintenance systems are even beginning to implement machine learning to determine when asset failure may be imminent and prescribe preventive measures.

![Industry 4.0](https://www.wipro.com/content/dam/nexus/en/service-lines/product-engineering/infographics/iot-in-the-manufacturing-industry-enabling-industry-4-0-1.png)

Now, this doesn’t mean we weren’t using data before—we’ve been gathering data for decades. The difference is simply the sheer volume of data available and the new methods we have for handling it all.

IIoT with CPS and cloud-based processes allow us to gather and interpret data in ways that weren’t possible before, and the impact of those technologies is being felt in every aspect of manufacturing, from production to maintenance to marketing, and even then on to the final products we create.

By all this advantage and techinical benefits, Industry 4.0 is the way of the future.


